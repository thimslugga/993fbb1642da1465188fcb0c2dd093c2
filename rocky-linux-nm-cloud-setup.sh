#!/usr/bin/env bash

sudo dnf install -y NetworkManager-cloud-setup

sudo systemctl enable --now nm-cloud-setup.service
sudo systemctl enable --now nm-cloud-setup.timer

sudo mkdir -pv /etc/systemd/system/nm-cloud-setup.service.d

cat <<'EOF' | sudo tee /etc/systemd/system/nm-cloud-setup.service.d/00-override.conf

[Service]
Environment=NM_CLOUD_SETUP_EC2=yes

EOF

sudo systemctl daemon-reload
sudo systemctl try-reload-or-restart nm-cloud-setup.service